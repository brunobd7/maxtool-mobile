import 'package:bloc/bloc.dart';
import 'package:maxtool_mobile/pages/home.dart';
import 'package:maxtool_mobile/pages/login_logoff.dart';
import 'package:maxtool_mobile/pages/tarefas.dart';
import 'package:maxtool_mobile/pages/tickets.dart';

enum EventosNavegacao {
  HomeClickEvent,
  TarefasClickEvent,
  TicketsClickEvent,
  DesconectarClickEvent
}

abstract class EstadosNavegacao {}

class NavegacaoBloc extends Bloc<EventosNavegacao, EstadosNavegacao> {
  @override
  EstadosNavegacao get initialState =>
      Home(); //ADICIONADO 'WHIT' PARA RETORNO SER COMPATIVEL COM O TIPO 'ESTADOSNAVEGACAO'

  @override //MAPEANDO CASOS DE NAVEGACAO
  Stream<EstadosNavegacao> mapEventToState(eventoNavegacao) async* {
    //async* SEMPRE RETORNA UM OBJETO STREAM E UM VALOR PARA USAR O yield
    //yield retorna algo sem finalizar o fluxo da funcao, semelhante ao return(java)
    switch (eventoNavegacao) {
      case EventosNavegacao.HomeClickEvent:
        yield Home();
        break;
      case EventosNavegacao.TarefasClickEvent:
        yield Tarefas();
        break;
      case EventosNavegacao.DesconectarClickEvent:
        yield LoginLogoff();
        break;
      case EventosNavegacao.TicketsClickEvent:
        yield Tickets();
        break;
    }
  }
}
