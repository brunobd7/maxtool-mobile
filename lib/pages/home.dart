import 'package:flutter/material.dart';
import 'package:maxtool_mobile/navegacao_bloc/navegacao_bloc.dart';

class Home extends StatelessWidget with EstadosNavegacao{
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "Pagina Inicial",
        style: TextStyle(fontWeight: FontWeight.w900, fontSize: 28),
      ),
    );
  }
}
