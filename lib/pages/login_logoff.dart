import 'package:flutter/material.dart';
import 'package:maxtool_mobile/navegacao_bloc/navegacao_bloc.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:maxtool_mobile/sidebar/sidebar_layout.dart';

class LoginLogoff extends StatelessWidget with EstadosNavegacao {
  Duration get loginTime => Duration(milliseconds: 2250);

  static const users = const {
    'maxtool@maxtool.com.br': '123',
    'bruno@maxtool.com.br': '123',
  };

  Future<String> _authLogin(LoginData data){

    print('Name: ${data.name}, Password: ${data.password}');
    return Future.delayed(loginTime).then((_) {
      if (!users.containsKey(data.name)) {
        return 'Usuario invalido';
      }
      if (users[data.name] != data.password) {
        return 'Senha incorreta';
      }
      return null;
    });

  }

  @override
  Widget build(BuildContext context) {
    return FlutterLogin(
      title: '',
      logo: 'images/logo_maxtool_2.png',
//      onLogin: (_) => Future(null),
//      onSignup: (_) => Future(null),
      onLogin: _authLogin,
      onSignup: _authLogin,

      onSubmitAnimationCompleted: () {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => SideBarLayout(),
        ));
      },
      onRecoverPassword: (_) => Future(null),

      messages: LoginMessages(
          usernameHint: 'Usuario',
          passwordHint: 'Senha',
          confirmPasswordHint: 'Confirme a senha',
          forgotPasswordButton: 'Esqueceu sua senha?',
          signupButton: 'CADASTRAR-SE',
          goBackButton: 'VOLTAR',
          recoverPasswordButton: 'RECUPERAR',
          recoverPasswordDescription:
              'Nova senha sera enviada para o email cadastrado.',
          recoverPasswordIntro: 'Recupere sua senha aqui'),

      theme: LoginTheme(
        primaryColor: const Color(0xFF262AAA),
        pageColorDark: Colors.cyan,
//        titleStyle: TextStyle(fontFamily: 'Quicksand', letterSpacing: 3),
      ),
    );
  }
}
