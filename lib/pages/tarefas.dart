import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:maxtool_mobile/models/tarefa.dart';
import 'package:maxtool_mobile/navegacao_bloc/navegacao_bloc.dart';
import 'package:http/http.dart' as http;

Future<List<Tarefa>> listarComApi() async {
  //metodo usando a modelagem e convertendo o json da api de acordo com a modelagem

  final response = await http
      .get("http://apresentacao.maxtool.com.br/api/v1/activities/department/1");

  if (response.statusCode == 200) {
    //HTTP CODE OK
    return compute(mapearListagem, response.body);

  } else {
    throw Exception("Falha ao carregar tarefas");
  }
}

List<Tarefa> mapearListagem(String responseBody){

  final listaTarefas =  json.decode(responseBody);

  return ResultTarefa.fromJson(listaTarefas).results;

}

class Tarefas extends StatelessWidget with EstadosNavegacao {
  final List<Tarefa> tarefas;

  Tarefas({Key key, this.tarefas}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: FutureBuilder<List<Tarefa>>(
            future: listarComApi(),
            builder: (context, snapshot) {
              if (snapshot.hasError) print(snapshot.error);

              if (snapshot.hasData) {
                return ListarTarefas(
                  tarefas: snapshot.data,
                );
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            }),
      ),
    );
  }
}

class ListarTarefas extends StatelessWidget{

  final List<Tarefa> tarefas;

  ListarTarefas({Key key, this.tarefas}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: tarefas.length,
        itemBuilder: (context,index){
          return Card(
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage('images/icon_maxtool_2.png'),
                radius: 26,
                backgroundColor: Colors.black,
              ),
              title: Text(tarefas[index].title),
              subtitle: Text(
                tarefas[index].description,
                overflow: TextOverflow.ellipsis,
                maxLines: 3,
              ),
            ),
          );
        }
    );
  }

}
