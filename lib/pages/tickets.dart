
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maxtool_mobile/navegacao_bloc/navegacao_bloc.dart';

class Tickets extends StatelessWidget with EstadosNavegacao{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:SafeArea(
          child: FormularioChamados(),
      ),
    );
  }

}


class FormularioChamados extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'Titulo...',
                ),

                validator:(value){
                  if(value.isEmpty){
                    return 'Insira o titulo do chamado!';
                  }
                  return null;
                },
              ),
              DropdownButtonFormField(
                items: <String>['ADM','Compras','RH']
                    .map<DropdownMenuItem<String>>((String value){
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: null,
                icon: Icon(Icons.arrow_downward),
                hint: Text('Selecione o tipo'),
              ),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'Descricao...',
                  hintMaxLines: 5,
                ),
              ),
              RaisedButton(
                onPressed: () {

                },
                child: Text('Gerar Chamado'),

              ),
            ],
          ),
    );
  }

}
