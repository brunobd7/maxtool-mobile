import 'package:flutter/material.dart';

class MenuItem extends StatelessWidget {
  final IconData icone;
  final String titulo;
  final Function onTap;

  //CONTRUTOR PARA INICIALIZAR VARIOS ITENS DE MENU SEGUINDO LAYOUT MONTADO
  const MenuItem({Key key, this.icone, this.titulo, this.onTap}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Row(
          children: <Widget>[
            Icon(
              icone,
              color: Colors.cyan,
              size: 30,
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              titulo,
              style: TextStyle(
                  fontWeight: FontWeight.w400, fontSize: 24, color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}
