import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maxtool_mobile/navegacao_bloc/navegacao_bloc.dart';
import 'package:maxtool_mobile/sidebar/menu_item.dart';
import 'package:rxdart/rxdart.dart';

class SideBar extends StatefulWidget {
  @override
  _SideBarState createState() => _SideBarState();
}

class _SideBarState extends State<SideBar>
    with SingleTickerProviderStateMixin<SideBar> {
  //CONTROLE DA ANIMAÇAO DE ABERTURA E FECHAMETNO DO MENU
  AnimationController _animationController;
  StreamController<bool> barraEstaExpandidaStreamController;
  Stream<bool> barraEstaExpandidaStream;
  StreamSink<bool> barraEstaExpandidaSink;

//  final bool barraEstaExpandida = false;
  final _duracaoAnimacaoBarra = const Duration(milliseconds: 500);

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: _duracaoAnimacaoBarra);
    barraEstaExpandidaStreamController = PublishSubject<
        bool>(); //LIB COM METODOS ADICIONAIS PARA AS STREAM DO DART
    barraEstaExpandidaStream = barraEstaExpandidaStreamController.stream;
    barraEstaExpandidaSink = barraEstaExpandidaStreamController.sink;
  }

  @override
  void dispose() {
    _animationController.dispose();
    barraEstaExpandidaStreamController.close();
    barraEstaExpandidaSink.close();
    super.dispose();
  }

  void onIconPressed() {
    //EVENTO PARA 'CLICKS'
    final statusAnimacao = _animationController.status;
    final animacaoCompletouExecucao =
        statusAnimacao == AnimationStatus.completed;

    //SE ANIMACAO FOI COMPLETADA QUER DIZER QUE O MENU ESTA ABERTO
    if (animacaoCompletouExecucao) {
      barraEstaExpandidaSink.add(false);
      _animationController.reverse();
    } else {
      barraEstaExpandidaSink.add(true);
      _animationController.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    //INICIALIZAÇÃO DO WIDGET
    final tamanhoTela =
        MediaQuery.of(context).size.width; //CAPTURA TAMANHO TELA DEVICE

    //USANDO STREAM BUILDER PARA RETORNAR MENU COM ANIMÇAO TRABALHAR VIA STREAM
    return StreamBuilder<bool>(
      initialData: false,
      stream: barraEstaExpandidaStream,
      builder: (context, barraEstaExpandidaAsync) {
        return AnimatedPositioned(
          duration: _duracaoAnimacaoBarra,
          top: 0,
          bottom: 0,
          left: barraEstaExpandidaAsync.data ? 0 : -tamanhoTela,
          right: barraEstaExpandidaAsync.data ? 0 : tamanhoTela - 45,
          //OCULTANDO DA DIREITA PARA ESQUERDA

          child: SafeArea(
            child: Row(
              children: <Widget>[
                Expanded(
                  //MENU EXPANDIDO
                  child: Container(
                    color: const Color(0xFF262AAA),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 100,
                        ),
                        ListTile(
                          title: Text(
                            "Administrador",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 30,
                                fontWeight: FontWeight.w800),
                          ),
                          subtitle: Text(
                            "maxtool@maxtool.com.br",
                            style: TextStyle(
                                color: Color(0xFF1BB5FD), fontSize: 16),
                          ),
                          leading: CircleAvatar(
                            child: Icon(
                              Icons.perm_identity,
                              color: Colors.white,
                            ),
                            radius: 40,
                          ),
                        ),
                        Divider(
                          height: 32,
                          thickness: 0.5,
                          color: Colors.white.withOpacity(0.3),
                          indent: 16,
                          endIndent: 16,
                        ),
                        MenuItem(
                          icone: Icons.home,
                          titulo: "Home",
                          onTap: () {
                            onIconPressed(); //FECHANDO SIDEBAR AO ABRIR OUTRA TELA
                            BlocProvider.of<NavegacaoBloc>(context).add(
                                EventosNavegacao
                                    .HomeClickEvent); //ADICIONANDO EVENTO CRIADO NO BLOC DE NAVEGACAO
                          },
                        ),
                        MenuItem(
                          icone: Icons.format_list_bulleted,
                          titulo: "Atividades",
                          onTap: () {
                            onIconPressed(); //FECHANDO SIDEBAR AO ABRIR OUTRA TELA
                            BlocProvider.of<NavegacaoBloc>(context).add(
                                EventosNavegacao
                                    .TarefasClickEvent); //ADICIONANDO EVENTO CRIADO NO BLOC DE NAVEGACAO
                          },
                        ),
                        MenuItem(
                          icone: Icons.library_add,
                          titulo: "Tickets/Chamados",
                          onTap: (){
                            onIconPressed();
                            BlocProvider.of<NavegacaoBloc>(context)
                                .add(EventosNavegacao.TicketsClickEvent);
                          },
                        ),
                        MenuItem(
                          icone: Icons.help,
                          titulo: "Ajuda/Guias",
                        ),
                        MenuItem(
                          icone: Icons.power_settings_new,
                          titulo: "Desconectar",
                          onTap: () {
                            onIconPressed(); //FECHANDO SIDEBAR AO ABRIR OUTRA TELA
                            BlocProvider.of<NavegacaoBloc>(context).add(
                                EventosNavegacao
                                    .DesconectarClickEvent); //ADICIONANDO EVENTO CRIADO NO BLOC DE NAVEGACAO
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment(0, -0.9),
                  child: GestureDetector(
                    //CAPTURA GESTOS NA TELA
                    onTap: () {
                      onIconPressed();
                    },
                    child: ClipPath(
                      clipper: MenuClipper(),
                      child: Container(
                        width: 35,
                        height: 110,
                        color: Color(0xFF262AAA),
                        alignment: Alignment.centerLeft,
                        child: AnimatedIcon(
                          //ICONE DA ABA DE  MENU
                          icon: AnimatedIcons.menu_close,
                          progress: _animationController.view,
                          color: Color(0xFF1BB5FD),
                          size: 25,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class MenuClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Paint paint = Paint();
    paint.color =
        Colors.white; //COR QUE SERA PINTADO O CONTAINER PARA DAR FORMA

    //altura e comprimento do container sera passado por parametro
    final width = size.width;
    final height = size.height;

    Path path = Path();

    path.moveTo(0, 0);
    path.quadraticBezierTo(0, 8, 10, 16);
    path.quadraticBezierTo(width - 1, height / 2 - 20, width, height / 2);
    path.quadraticBezierTo(width + 1, height / 2 + 20, 10, height - 16);
    path.quadraticBezierTo(0, height - 8, 0, height);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
