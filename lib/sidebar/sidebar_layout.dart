import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maxtool_mobile/navegacao_bloc/navegacao_bloc.dart';
import 'package:maxtool_mobile/pages/home.dart';
import 'package:maxtool_mobile/pages/login_logoff.dart';
import 'package:maxtool_mobile/pages/minha_conta.dart';
import 'package:maxtool_mobile/pages/tarefas.dart';
import 'package:maxtool_mobile/sidebar/sidebar.dart';

class SideBarLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //WIDGET 'STACK' DE SOBREPOSIÇAO E AGRUPAMENTO DE WIDGETS , FUGINDO DO LAYOUT BASICOS LISTAS, COLUNAS ,CONTAINERS
      body: BlocProvider<NavegacaoBloc>(
        create: (context) => NavegacaoBloc(),
        child: Stack(
          children: <Widget>[
            BlocBuilder<NavegacaoBloc, EstadosNavegacao>(
              builder: (context, estadoNavegacao){
                return estadoNavegacao as Widget;
              },
            ),
            SideBar(),
          ],
        ),
      ),
    /*  body: Stack(
        children: <Widget>[
          Home(),
//          Tarefas(),
//          MinhaConta(),
//          LoginLogoff(),
          SideBar(),
        ],
      ),*/
    );
  }
}
