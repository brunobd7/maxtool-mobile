class ResultTarefa {
  List<Tarefa> results;

  ResultTarefa({this.results});

  ResultTarefa.fromJson(Map<String, dynamic> json) {
    if (json['results'] != null) {
      results = new List<Tarefa>();

      json['results'].forEach((v) {
        results.add(new Tarefa.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Tarefa {
  int id;
  String title;
  String description;
  String initialDate;
  String finalDate;
  String deadline;
  String expectedDuration;
  String accomplishedDuration;
  int priority;
  int activityTypeId;
  int activityStatusId;
  int clientId;
  int userId;
  int userResponsibleId;
  int departmentId;
  int requesterId;
  String startedAt;
  String endedAt;
  String valueTotal;
  String createdAt;
  String updatedAt;
  int automationId;

  Tarefa(
      {this.id,
      this.title,
      this.description,
      this.initialDate,
      this.finalDate,
      this.deadline,
      this.expectedDuration,
      this.accomplishedDuration,
      this.priority,
      this.activityTypeId,
      this.activityStatusId,
      this.clientId,
      this.userId,
      this.userResponsibleId,
      this.departmentId,
      this.requesterId,
      this.startedAt,
      this.endedAt,
      this.valueTotal,
      this.createdAt,
      this.updatedAt,
      this.automationId});

  Tarefa.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
    initialDate = json['initial_date'];
    finalDate = json['final_date'];
    deadline = json['deadline'];
    expectedDuration = json['expected_duration'];
    accomplishedDuration = json['accomplished_duration'];
    priority = json['priority'];
    activityTypeId = json['activity_type_id'];
    activityStatusId = json['activity_status_id'];
    clientId = json['client_id'];
    userId = json['user_id'];
    userResponsibleId = json['user_responsible_id'];
    departmentId = json['department_id'];
    requesterId = json['requester_id'];
    startedAt = json['started_at'];
    endedAt = json['ended_at'];
    valueTotal = json['value_total'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    automationId = json['automation_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['description'] = this.description;
    data['initial_date'] = this.initialDate;
    data['final_date'] = this.finalDate;
    data['deadline'] = this.deadline;
    data['expected_duration'] = this.expectedDuration;
    data['accomplished_duration'] = this.accomplishedDuration;
    data['priority'] = this.priority;
    data['activity_type_id'] = this.activityTypeId;
    data['activity_status_id'] = this.activityStatusId;
    data['client_id'] = this.clientId;
    data['user_id'] = this.userId;
    data['user_responsible_id'] = this.userResponsibleId;
    data['department_id'] = this.departmentId;
    data['requester_id'] = this.requesterId;
    data['started_at'] = this.startedAt;
    data['ended_at'] = this.endedAt;
    data['value_total'] = this.valueTotal;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['automation_id'] = this.automationId;
    return data;
  }
}
